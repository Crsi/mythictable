<!-- Title suggestion: [Story] Name of story -->

## User Story

As a player/user/DM/developer, I can ... 

## Details

Any related information

## Steps

- [ ] TBD

## Acceptance Criteria

- [ ] TBD


/label ~"Story"
/milestone %"First Playable" 
/weight 2
<!-- A note on weights
0 is trivial. Does not involve and code
1 is a button, documentation corrections, a quick fix
2 is a simple well known task
3 is for a somewhat simple task
5 is for a complicated story that requies a number of moving parts
8 is for a signifcant amount of work but should with perhaps some unkowns or external dependencies
Anything higher than 8 should be broken up into smaller more distinct stories.
-->