<!-- Title suggestion: [Onboarding] Newcomer's name -->

# Welcome to Mythic Table's Development Team!

We hope you're as excited as we are for being a part in this mythic project.

But before the fun part begins, we need to make sure you're ready to work alongside other developers while following certain guidelines that helps everyone to better understand the current state of development and keeps them up to date.

To achieve that, we need you to roll some checks that may take some time to complete:

## **1. Knowledge (Int - DC 10)**
**The first step into your mythical journey is to know the path itself.**

### Required

Learn the story behind the project through these ancient tomes from the developers on the main website. Make sure to be familiar with the following:
- [ ] [*Main Site*](https://www.mythictable.com/)
- [ ] [*Non-profit Announcement*](https://www.mythictable.com/hotlinks/now-non-profit)
- [x] // Todo - Broken Link [*Mission Statement*](https://www.mythictable.com/mission)
- [x] // Todo - Broken Link [*Product Features*](https://www.mythictable.com/features)
- [ ] [*Bookmark the MT's wiki*](https://gitlab.com/mythicteam/mythictable/-/wikis/home)

### Optional

- [ ] [*Mythic Table Dev Stories*](https://www.mythictable.com/mythicstories/index)
- [ ] [*Mythic Table Open Source Announcement*](https://www.mythictable.com/hotlinks/now-open)

### Optional - Get Social
Community is VERY important to us at Mythic Table. If you would like to get involved, join these groups:
- [ ] ![alt text](https://i.imgur.com/TtIyDnO.png "Youtube") - [*Youtube* (Help us get at least 100 subscribers)](https://www.youtube.com/mythictable)
- [ ] ![alt text](https://i.imgur.com/RBSCNgo.png "Facebook") - [*Facebook*](https://www.facebook.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/M9RR4Hf.png "Instagram") - [*Instagram*](https://www.instagram.com/mythic_table/)
- [ ] ![alt text](https://i.imgur.com/hgpY9gu.png "Reddit") - [*Reddit*](https://www.reddit.com/r/mythictable/)
- [ ] ![alt text](https://i.imgur.com/rEAMp9o.png "Twitter") - [*Twitter*](https://twitter.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/kX34PqK.png "Patreon") - [*Patreon*](https://www.patreon.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/m9k88St.png "Tumblr") - [*Tumblr*](https://mythictable.tumblr.com/)
- [ ] ![alt text](https://i.imgur.com/EbNeByg.png "Product Hunt") - [*Product Hunt*](https://www.producthunt.com/posts/mythic-table/)


## **2. Diplomacy (Cha - DC 12)**
Working together as a team requires a lot of diplomacy. In an attempt to center and help you to communicate with everybody, we use an magical artifact called [**Slack**](https://slack.com/).

Through Slack, we can make sure everyone can stay up-to-date with the whole project, while being able to get to know your coworkers. That's accomplished through the division of channels that Slack provides. Each channel is used to a specific purpose: the General channel is used for announcements and welcome messages. The Mythic Table is our all purpose meeting place. This is usually reserved for important business matters but it's a good place to start if you don't know where else to turn. While the Random channel is for any chat that doesn't concern Mythic Table at all. This division of subjects makes everything organized and lets you get in touch with everyone on the team.

Follow these instructions to get acquainted with Slack
- [ ] Join the **Random** channel and tell us about yourself
- [ ] Reply to or start a **Thread**
- [ ] Send Marc a private message
- [ ] Customize your notifications

Chose one:
- [ ] Programmer - Join the dev channel on slack
- [ ] Operations or DevOps - Join the devops channel on slack
- [ ] Artist - Join the art channel on slack
- [ ] Designer - Join the design channel on slack
- [ ] Community manager - Join the community channel on slack
- [ ] Other - Join the dev channel on slack

Other useful slack channels:
- [ ] Daily
- [ ] Learn

### Optional
- [ ] Get Slack on your phone

## **3. Profession ___________ (DC 14)**

Getting up to speed with how to use Gitlab for project planning is not difficult, but it will take a little practice. There are a couple of really great resources that will be useful.

- [ ] Understanding the Milestones: https://gitlab.com/mythicteam/mythictable/-/milestones
- [ ] Using the Kanban Board: https://gitlab.com/mythicteam/mythictable/-/boards/
- [ ] Read our guidelines: https://gitlab.com/mythicteam/mythictable/-/wikis/workflow-guidelines


## **4. Use Magic Device (Int - DC 16)**
Our Chronomancers are the ones responsible in case something goes wrong. They use a magic called [**Git**](https://git-scm.com/) that allows them to return in any point in time so that we're safe from mistakes.

Git allows the developers to manage the versioning of the project, letting them create different "timelines (branches)". This means that everyone can work on different parts of the project and when they're done, they can merge these "timelines" to create a new one.

**Since you're joining our forces, you'll also become a Chronomancer.**

If you have no knowledge on the art of bending time to your will, give a quick look at these sources that can shed a light on the subject:

- [ ] [*Try a tutorial*](https://try.github.io/)

0r
- [ ] Already a pro

Once you're familiar with the idea behind Git, it's time for some real training...

**To make it official, you're going to sign your name on the Contributors.md page by following the instructions bellow: **

> *If you need any help or have doubts don't be afraid to ask in the #dev channel on slack.* 

- [ ] Start by reading the [*guidelines*](https://gitlab.com/mythicteam/mythictable/-/wikis/how-to/branching)
- [ ] Clone the Git repo `git clone git@gitlab.com:mythicteam/mythictable.git`
- [ ] Create a new branch `git checkout -b issue/##-your-name-onboarding`
- [ ] Find and edit CONTRIBUTORS.md - Add your name ;)
- [ ] Stage your changes `git add CONTRIBUTORS.md`
- [ ] Commit your changes `git commit -m "Issue #??: Adding myself to CONTRIBUTORS.md"`
- [ ] Push your changes `git push -u origin issue/##-your-name-onboarding`

Next you'll create a merge request:


 ![alt text](https://i.imgur.com/xL3P4D5.png) 

- [ ] Visit https://gitlab.com/mythicteam/mythictable/-/merge_requests/new
- [ ] Be sure to use a template for your Merge Request
- [ ] Make sure to add your issue number in the `Closes #??` part
- [ ] Ask someone to review it on slack
- [ ] Sqaush, Merge and Delete your branch. You're done!

## 5. Extra Credit

- [ ] Read the [*Workflow Guidelines*](https://gitlab.com/mythicteam/mythictable/-/wikis/workflow-guidelines) 
- [ ] Read the [*WIP Code Conventions*](https://gitlab.com/mythicteam/mythictable/-/wikis/coding-guidelines) 
- [ ] Create a new ticket/bug in Gitlab

