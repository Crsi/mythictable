import { shallowMount, createLocalVue } from '@vue/test-utils';
import Library from '@/components/common/library/Library.vue';

import * as VueWindow from '@hscmap/vue-window';

const localVue = createLocalVue();
localVue.use(VueWindow);

describe('Generic Library component', () => {
    it('should emit an event on an item being added', () => {
        const wrapper = shallowMount(Library, { localVue });
        const addInput = wrapper.find('input');
        addInput.trigger('change');

        expect(wrapper.emitted('add-item')).toBeTruthy();
    });
});
