import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import Scene from '@/components/scene/Scene.vue';
import PlayToken from '@/components/scene/PlayToken.vue';
import GameStateStore from '@/store/GameStateStore';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        gamestate: GameStateStore,
    },
});

describe('Scene component', () => {
    const stubs = {
        PlayToken,
    };
    const wrapper = shallowMount(Scene, { localVue, store, stubs });
    wrapper.vm.$refs['playTokens'] = [
        { entity: { id: 1, selected: false } },
        { entity: { id: 2, selected: false } },
        { entity: { id: 3, selected: false } },
    ];
    const vm = wrapper.vm;
    it('should have a PlayToken stub', () => {
        const playToken = wrapper.find('playToken');
        expect(playToken).toBeDefined();
    });
    it('should have 3 "playTokens" as a $ref', () => {
        expect(wrapper.vm.$refs['playTokens'].length).toEqual(3);
    });

    describe('default', () => {
        describe('.renderContext', () => {
            const context = vm.renderContext;

            it('is defined', () => {
                expect(context).toBeDefined();
            });

            it('has gridSpace property', () => {
                expect(context.gridSpace).toBeDefined();
            });

            // Skipping for the #67 change
            it.skip('has default gridSpace matching configs from stage', () => {
                expect(context.gridSpace.type).toEqual(vm.stage.grid.type);
                expect(context.gridSpace.size).toEqual(vm.stage.grid.size);
            });

            it('has pixelRatio', () => {
                expect(vm.renderContext.pixelRatio).toEqual(1);
            });
        });

        describe('.stage', () => {
            const stage = vm.stage;

            describe('has default stage...', () => {
                it('size is 50x50', () => {
                    expect(stage.bounds).toEqual({
                        nw: { q: 0, r: 0 },
                        se: { q: 49, r: 49 },
                    });
                });

                it('with square grid of size 50', () => {
                    expect(stage.grid.type).toEqual('square');
                    expect(stage.grid.size).toEqual(50);
                });
            });
        });

        describe('.entities', () => {
            const entities = vm.entities;

            it('exist', () => {
                expect(entities).toBeDefined();
            });
        });

        describe('.selectedTokenId', () => {
            const selectedTokenId = vm.selectedTokenId;

            it('exists', () => {
                expect(selectedTokenId).toBeDefined();
            });

            it('should be blank by default', () => {
                expect(selectedTokenId).toEqual('');
            });
            it('should update "onTokenSelected"', () => {
                vm.onTokenSelected({ entity: { id: 2, selected: true } });
                expect(vm.selectedTokenId).toEqual(2);
                let mockTargetToken = wrapper.vm.$refs['playTokens'].find(token => token.entity.id === 2);
                vm.onTokenSelected({ entity: { id: 1 } });
                expect(vm.selectedTokenId).toEqual(1);
                expect(mockTargetToken.entity.selected).toBeFalsy();
            });
        });
    });

    describe('with non-default stage', () => {
        const wrapper = shallowMount(Scene, { localVue, store });
        const vm = wrapper.vm;
        const gridSize = 10;

        vm.stage.grid = { size: gridSize };

        // Skipping this because of a hack  to resize the maps for the stream
        test.skip('gridSpace size matches stage after map is changed', () => {
            expect(vm.renderContext.gridSpace.size).toEqual(gridSize);
        });
    });
});
