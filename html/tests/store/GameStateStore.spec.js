import Vuex from 'vuex';
import { createLocalVue } from '@vue/test-utils';
import GameStateStore from '@/store/GameStateStore';

const storeConfig = GameStateStore;
storeConfig.namespaced = false;

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store(storeConfig);

const resetState = store => {
    store.replaceState({
        base: {
            entities: {
                entity1: 'foo',
                entity2: 'bar',
            },
            global: {
                global1: 'foo',
                global2: 'bar',
                global3: [],
            },
            ruleset: {},
        },
        entities: {
            entity1: 'foo',
            entity2: 'bar',
        },
        global: {
            global1: 'foo',
            global2: 'bar',
            global3: [],
        },
        deltas: [],
        undoneDeltas: [],
        ruleset: {},
    });
};

describe('GameStateStore', () => {
    describe('getters', () => {});
    describe('mutations', () => {
        describe('addDelta(delta)', () => {
            beforeEach(() => {
                resetState(store);
            });

            const delta = { op: 'add', path: 'foo/bar', value: 'baz' };
            const deltas = [{ op: 'replace', path: 'foo/bar', value: 'test' }, { op: 'remove', path: 'foo/bar' }];
            it('adds no delta when delta is empty', () => {
                store.commit('addDelta', null);
                expect(store.state.deltas).toEqual([]);
            });
            it('adds a delta when delta is object', () => {
                store.commit('addDelta', delta);
                expect(store.state.deltas).toEqual([[{ op: 'add', path: 'foo/bar', value: 'baz' }]]);
            });
            it('adds a delta when delta is array', () => {
                store.commit('addDelta', deltas);
                expect(store.state.deltas).toEqual([
                    [{ op: 'replace', path: 'foo/bar', value: 'test' }, { op: 'remove', path: 'foo/bar' }],
                ]);
            });
            it('adds multiple deltas in correct order', () => {
                store.commit('addDelta', delta);
                store.commit('addDelta', deltas);
                expect(store.state.deltas).toEqual([
                    [{ op: 'add', path: 'foo/bar', value: 'baz' }],
                    [{ op: 'replace', path: 'foo/bar', value: 'test' }, { op: 'remove', path: 'foo/bar' }],
                ]);
            });
        });
        describe('resetGameState()', () => {
            it('resets removed properties', () => {
                store.commit('resetGameState');
                expect(store.state.entities).toEqual(store.state.base.entities);
                expect(store.state.global).toEqual(store.state.base.global);
            });
            it('resets changed properties', () => {
                store.state.global.global1 = 'baz';
                store.state.entities.entity1 = 'baz';

                expect(store.state.global.global1).toBe('baz');
                expect(store.state.entities.entity1).toBe('baz');

                store.commit('resetGameState');

                expect(store.state.entities).toEqual(store.state.base.entities);
                expect(store.state.global).toEqual(store.state.base.global);
            });
            it('removes added properties', () => {
                store.state.global.global4 = 'baz';
                store.state.entities.entity3 = 'baz';

                expect(store.state.global).toEqual({
                    global1: 'foo',
                    global2: 'bar',
                    global3: [],
                    global4: 'baz',
                });
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'bar',
                    entity3: 'baz',
                });

                store.commit('resetGameState');

                expect(store.state.entities).toEqual(store.state.base.entities);
                expect(store.state.global).toEqual(store.state.base.global);
            });
        });
    });
    describe('actions', () => {
        beforeEach(() => {
            resetState(store);
        });

        describe('setBase()', () => {
            it('Updates the base to the current state', () => {
                let newBaseEntities = {
                    ent: 1,
                    ent2: '2',
                };
                let newBaseGlobal = {
                    glob: 1,
                    glob2: '2',
                };

                store.state.entities = newBaseEntities;
                store.state.global = newBaseGlobal;

                store.dispatch('setBase');

                expect(store.state.base.entities).toEqual(newBaseEntities);
                expect(store.state.base.global).toEqual(newBaseGlobal);
            });
        });
        describe('applyDelta(delta)', () => {
            const delta = [{ op: 'add', path: '/entities/entity3', value: 'baz' }];
            const op = { op: 'remove', path: '/entities/entity2' };
            const multiDelta = [
                [{ op: 'add', path: '/entities/entity3', value: 'baz' }],
                [{ op: 'remove', path: '/entities/entity2' }],
            ];

            it('Applies delta with a single add', () => {
                store.dispatch('applyDelta', delta);
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'bar',
                    entity3: 'baz',
                });
                expect(store.state.deltas).toEqual([delta]);
            });
            it('Applies a delta consisting of a single operation', () => {
                store.dispatch('applyDelta', op);
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                });
                expect(store.state.deltas).toEqual([[op]]);
            });
            it('applies a delta with multiple patches', () => {
                store.dispatch('applyDelta', multiDelta);
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity3: 'baz',
                });
            });
        });
        describe('resolveAction(action)', () => {
            // TODO: Implement this after actions are modeled clearer
        });
        describe('patch(patch)', () => {
            describe('with valid operations', () => {
                it('can do add operations', () => {
                    const patch = [
                        { op: 'add', path: '/entities/entity3', value: 'baz' },
                        { op: 'add', path: '/global/global4', value: {} },
                        { op: 'add', path: '/global/global4/goo', value: 'gaa' },
                    ];
                    store.dispatch('patch', patch);

                    expect(store.state.entities).toEqual({
                        entity1: 'foo',
                        entity2: 'bar',
                        entity3: 'baz',
                    });
                    expect(store.state.global).toEqual({
                        global1: 'foo',
                        global2: 'bar',
                        global3: [],
                        global4: {
                            goo: 'gaa',
                        },
                    });
                });
                it('can do remove operations', () => {
                    const patch = { op: 'remove', path: '/entities/entity2' };
                    store.dispatch('patch', patch);
                    expect(store.state.entities).toEqual({
                        entity1: 'foo',
                    });
                });
                it('can do replace operations', () => {
                    const patch = { op: 'replace', path: '/entities/entity2', value: 'baz' };
                    store.dispatch('patch', patch);
                    expect(store.state.entities).toEqual({
                        entity1: 'foo',
                        entity2: 'baz',
                    });
                });
                it('can do move operations', () => {
                    const patch = { op: 'move', from: '/entities/entity2', path: '/entities/entity3' };
                    store.dispatch('patch', patch);
                    expect(store.state.entities).toEqual({
                        entity1: 'foo',
                        entity3: 'bar',
                    });
                });
                it('can do copy operations', () => {
                    const patch = { op: 'copy', from: '/entities/entity2', path: '/entities/entity3' };
                    store.dispatch('patch', patch);
                    expect(store.state.entities).toEqual({
                        entity1: 'foo',
                        entity2: 'bar',
                        entity3: 'bar',
                    });
                });
                it('can do successful test operations', () => {
                    const patch = { op: 'test', path: '/entities/entity2', value: 'bar' };
                    store.dispatch('patch', patch);
                });
                it('can add a new list', () => {
                    const patch = { op: 'add', path: '/global/global4', value: [] };
                    store.dispatch('patch', patch);
                    expect(store.state.global.global4).toEqual([]);
                });
                it('can add an item to a list', () => {
                    const patch = { op: 'add', path: '/global/global3/0', value: 'foo' };
                    store.dispatch('patch', patch);
                    expect(store.state.global.global3).toEqual(['foo']);
                });
                it('can add an item to a specific index in a list', () => {
                    const patch = [
                        { op: 'add', path: '/global/global3/0', value: 'foo' },
                        { op: 'add', path: '/global/global3/1', value: 'bar' },
                        { op: 'add', path: '/global/global3/1', value: 'baz' },
                    ];
                    store.dispatch('patch', patch);
                    expect(store.state.global.global3).toEqual(['foo', 'baz', 'bar']);
                });
                it('can add an item to the first index in a list', () => {
                    const patch = [
                        { op: 'add', path: '/global/global3/0', value: 'foo' },
                        { op: 'add', path: '/global/global3/0', value: 'bar' },
                    ];
                    store.dispatch('patch', patch);
                    expect(store.state.global.global3).toEqual(['bar', 'foo']);
                });
                it('can add an item to the end of a list', () => {
                    let patch = { op: 'add', path: '/global/global3/-', value: 'foo' };
                    store.dispatch('patch', patch);
                    expect(store.state.global.global3).toEqual(['foo']);

                    patch = { op: 'add', path: '/global/global3/-', value: 'bar' };
                    store.dispatch('patch', patch);
                    expect(store.state.global.global3).toEqual(['foo', 'bar']);
                });
                it('can remove an item from the beginning of a list', () => {
                    let patch = [
                        { op: 'add', path: '/global/global3/0', value: 'foo' },
                        { op: 'add', path: '/global/global3/1', value: 'bar' },
                    ];
                    store.dispatch('patch', patch);
                    expect(store.state.global.global3).toEqual(['foo', 'bar']);

                    patch = { op: 'remove', path: '/global/global3/0' };
                    store.dispatch('patch', patch);
                    expect(store.state.global.global3).toEqual(['bar']);
                });
                it('can remove an item from the end of a list', () => {
                    let patch = [
                        { op: 'add', path: '/global/global3/0', value: 'foo' },
                        { op: 'add', path: '/global/global3/1', value: 'bar' },
                    ];
                    store.dispatch('patch', patch);
                    expect(store.state.global.global3).toEqual(['foo', 'bar']);

                    patch = { op: 'remove', path: '/global/global3/1' };
                    store.dispatch('patch', patch);
                    expect(store.state.global.global3).toEqual(['foo']);
                });
            });
            describe('with illegal operations', () => {
                it('Throws exceptions when trying to remove forbidden paths', () => {
                    let patch = { op: 'remove', path: '/entities' };
                    let f = () => store.dispatch('patch', patch);
                    expect(f).toThrow(Error);

                    patch = { op: 'remove', path: '/global' };
                    f = () => store.dispatch('patch', patch);
                    expect(f).toThrow(Error);

                    patch = { op: 'remove', path: '/deltas' };
                    f = () => store.dispatch('patch', patch);
                    expect(f).toThrow(Error);
                });

                it('Throws exceptions when trying to move forbidden paths', () => {
                    let patch = { op: 'move', from: '/entities', path: '/global/test' };
                    let f = () => store.dispatch('patch', patch);
                    expect(f).toThrow(Error);

                    patch = { op: 'move', from: '/global', path: '/global/test' };
                    f = () => store.dispatch('patch', patch);
                    expect(f).toThrow(Error);

                    patch = { op: 'move', from: '/deltas', path: '/global/test' };
                    f = () => store.dispatch('patch', patch);
                    expect(f).toThrow(Error);
                });

                // TODO - Find out why we get this error `[Vue warn]: Cannot set reactive property on undefined, null, or primitive value: foo`
                it('Throws an exception when attempting to use list syntax on a non list entry', () => {
                    let patch = { op: 'add', path: '/entities/entity1/-', value: 'foo' };
                    let f = () => store.dispatch('patch', patch);
                    expect(f).toThrow(Error);
                    expect(store.state.entities).toEqual(store.state.base.entities);
                });
                it('throws an exception when adding to an out of bounds index in a list', () => {
                    const patch = { op: 'add', path: '/global/global3/4', value: 'foo' };
                    let f = () => store.dispatch('patch', patch);
                    expect(f).toThrow(Error);
                });
            });
        });
        describe('undo', () => {
            beforeEach(() => {
                store.dispatch('setBase');
            });

            it('can undo a single operation delta', () => {
                const delta = [
                    { op: 'add', path: '/entities/entity3', value: 'baz' },
                    { op: 'add', path: '/global/global4', value: 'goo' },
                    { op: 'replace', path: '/entities/entity2', value: 'gaa' },
                ];
                store.dispatch('applyDelta', delta);
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'gaa',
                    entity3: 'baz',
                });
                expect(store.state.global).toEqual({
                    global1: 'foo',
                    global2: 'bar',
                    global3: [],
                    global4: 'goo',
                });

                store.dispatch('undo');
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'bar',
                });
                expect(store.state.global).toEqual({
                    global1: 'foo',
                    global2: 'bar',
                    global3: [],
                });
            });
            it('can undo a multi operation delta', () => {
                const delta = [
                    [
                        { op: 'add', path: '/entities/entity3', value: 'baz' },
                        { op: 'add', path: '/global/global4', value: 'goo' },
                    ],
                    [{ op: 'replace', path: '/entities/entity2', value: 'gaa' }],
                ];

                store.dispatch('applyDelta', delta);
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'gaa',
                    entity3: 'baz',
                });
                expect(store.state.global).toEqual({
                    global1: 'foo',
                    global2: 'bar',
                    global3: [],
                    global4: 'goo',
                });

                store.dispatch('undo');
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'bar',
                });
                expect(store.state.global).toEqual({
                    global1: 'foo',
                    global2: 'bar',
                    global3: [],
                });
            });
            it('can undo multiple deltas', () => {
                const delta1 = [[{ op: 'add', path: '/entities/entity3', value: 'baz' }]];
                const delta2 = [{ op: 'replace', path: '/entities/entity2', value: 'gaa' }];

                store.dispatch('applyDelta', delta1);
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'bar',
                    entity3: 'baz',
                });

                store.dispatch('applyDelta', delta2);
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'gaa',
                    entity3: 'baz',
                });

                store.dispatch('undo');
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'bar',
                    entity3: 'baz',
                });

                store.dispatch('undo');
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'bar',
                });
            });
        });
        describe('redo', () => {
            beforeEach(() => {
                store.dispatch('setBase');
            });

            it('can redo a single undo', () => {
                const delta = [
                    { op: 'add', path: '/entities/entity3', value: 'baz' },
                    { op: 'replace', path: '/entities/entity2', value: 'gaa' },
                ];

                store.dispatch('applyDelta', delta);
                store.dispatch('undo');
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'bar',
                });

                store.dispatch('redo');
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'gaa',
                    entity3: 'baz',
                });
            });

            it('can redo a multiple undo', () => {
                const delta1 = [
                    { op: 'add', path: '/entities/entity3', value: 'baz' },
                    { op: 'replace', path: '/entities/entity2', value: 'gaa' },
                ];
                const delta2 = [{ op: 'replace', path: '/entities/entity2', value: 'goo' }];

                store.dispatch('applyDelta', delta1);
                store.dispatch('applyDelta', delta2);
                store.dispatch('undo');
                store.dispatch('undo');
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'bar',
                });

                store.dispatch('redo');
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'gaa',
                    entity3: 'baz',
                });

                store.dispatch('redo');
                expect(store.state.entities).toEqual({
                    entity1: 'foo',
                    entity2: 'goo',
                    entity3: 'baz',
                });
            });
        });
    });
});
