import axios from 'axios';
import { FILE_TYPES } from './constants';

export async function addFile(files, fileType) {
    if (!Object.values(FILE_TYPES).includes(fileType)) {
        throw 'Invalid type of file to add.';
    } else if (files && files.length) {
        const data = new FormData();
        data.append('files', files[0]);
        return axios.post(`/api/files?path=${fileType}`, data);
    } else {
        throw 'No files provided';
    }
}

export async function getFiles(fileType) {
    if (!Object.values(FILE_TYPES).includes(fileType)) {
        throw 'Invalid type of file to query.';
    } else {
        return axios.get(`/api/files?path=${fileType}`).then(result => {
            const { data } = result;
            if (Array.isArray(data)) {
                return data.map(image => {
                    return {
                        id: image.id,
                        asset: {
                            kind: 'image',
                            src: image.url,
                        },
                    };
                });
            }
            return [];
        });
    }
}

export async function addCharacter(files) {
    return addFile(files, FILE_TYPES.CHARACTER);
}

export async function getCharacters() {
    return getFiles(FILE_TYPES.CHARACTER);
}

export async function addMap(files) {
    return addFile(files, FILE_TYPES.MAP);
}

export async function getMaps() {
    return getFiles(FILE_TYPES.MAP);
}
