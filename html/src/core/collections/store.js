import Vue from 'vue';
import jsonpatch from 'fast-json-patch';
import { getByCampaign } from './api';
import { COLLECTION_TYPES } from './constants';
import Asset from '@/core/entity/Asset';

function getCollection(state, collection) {
    if (state.hasOwnProperty(collection)) {
        return state[collection];
    }
    throw new Error(`Could not find COLLECTION ${collection}`);
}

function getItem(state, collection, id) {
    const c = getCollection(state, collection);
    if (c.hasOwnProperty(id)) {
        return c[id];
    }
    throw new Error(`Could not find ITEM ${id} in COLLECTION ${collection}`);
}

const CollectionStore = {
    namespaced: true,
    state: {},
    getters: {
        getItem: state => (collection, id) => {
            return getItem(state, collection, id);
        },
        getCollection: state => collection => {
            return getCollection(state, collection);
        },
        scenes: state => () => {
            return getCollection(state, COLLECTION_TYPES.scenes);
        },
    },
    mutations: {
        patch(state, { collection, id, patch }) {
            const item = getItem(state, collection, id);
            jsonpatch.applyPatch(item, patch);
        },
        add(state, { collection, item }) {
            if (!state.hasOwnProperty(collection)) {
                Vue.set(state, collection, {});
            }
            Vue.set(state[collection], item._id, item);
        },
        remove(state, { collection, id }) {
            if (state.hasOwnProperty(collection)) {
                Vue.delete(state[collection], id);
            }
        },
    },
    actions: {
        async patch({ commit, dispatch, state }, parameters) {
            commit('patch', parameters);
            const item = getItem(state, parameters.collection, parameters.id);
            const promises = Asset.updateAll(item.stage.elements.map(e => e.asset));
            Promise.all(promises).then(() => {
                dispatch('gamestate/activateScene', item, { root: true });
            });
        },
        async load({ commit }, { collection, items }) {
            for (const i in items) {
                commit('add', { collection, item: items[i] });
            }
        },
        async add({ commit }, { collection, item }) {
            commit('add', { collection, item });
        },
        async remove({ commit }, { collection, id }) {
            commit('remove', { collection, id });
        },
        async loadScenes({ commit, rootState }) {
            await getByCampaign(COLLECTION_TYPES.scenes, rootState.live.sessionId).then(results => {
                for (const i in results) {
                    commit('add', { collection: COLLECTION_TYPES.scenes, item: results[i] });
                }
            });
        },
    },
};

export default CollectionStore;
