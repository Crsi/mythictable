import axios from 'axios';

export async function getByCampaign(collection, campaignId) {
    return axios.get(`/api/collections/${collection}/campaign/${campaignId}`).then(results => {
        return results.data;
    });
}
