// Use require() instead of import as SignalR does not use ES6-style export
const signalR = require('@microsoft/signalr');
import axios from 'axios';
import Asset from '@/core/entity/Asset';
import getAllImages from '@/components/scene/SceneHelper.js';
import { COLLECTION_TYPES } from '@/core/collections/constants';

class LivePlayDirector {
    constructor(store) {
        this.store = store;
        this.connection = null;
        store.commit('live/setDirector', this);
    }

    get sessionId() {
        return this.state.sessionId;
    }
    set sessionId(id) {
        this.state.sessionId = id;
    }

    get state() {
        return this.store.state.live;
    }

    async init() {
        this.store.commit('live/setUserId', this.store.state.oidcStore.user.given_name);

        let accessToken = this.store.state.oidcStore.access_token;
        this.connection = new signalR.HubConnectionBuilder()
            .withUrl('/api/live', { accessTokenFactory: () => accessToken })
            .withAutomaticReconnect()
            .build();

        this.connection.on('ConfirmOpDelta', this.onConfirmDelta.bind(this));
        this.connection.on('CharacterAdded', this.onCharacterAdded.bind(this));
        this.connection.on('CharacterRemoved', this.onCharacterRemoved.bind(this));
        this.connection.on('ExceptionRaised', this.onExceptionRaised.bind(this));
        this.connection.on('ReceiveDiceResult', this.onReceiveDiceResult.bind(this));
        this.connection.on('ObjectUpdated', this.onObjectUpdated.bind(this));
        this.connection.on('ObjectAdded', this.onObjectAdded.bind(this));
        this.connection.on('ObjectRemoved', this.onObjectRemoved.bind(this));
        this.connection.on('Undo', this.undo.bind(this));
        this.connection.on('Redo', this.redo.bind(this));
        this.connection.onclose(() => {
            // FIXME: PoC only; needs to be mutation if used in prod
            this.store.state.live.connected = false;
        });
    }

    async connect() {
        this.state.connected = this.connection.start();
        try {
            await this.state.connected;
        } catch (e) {
            this.state.connected = false;
            throw e;
        }
        this.state.connected = true; // FIXME: PoC only; needs to be mutation if used in prod
        await this.initializeEntities();
    }

    async disconnect() {
        await this.connection.stop();
        this.state.connected = false;
    }

    async initializeEntities() {
        axios
            .get(`/api/campaigns/${this.sessionId}/characters`)
            .then(response => {
                const characters = response.data;
                this.store.dispatch('gamestate/entities/load', characters);
                Asset.loadAll(characters);
            })
            .catch(error => {
                console.error(error);
            });
        axios
            .get(`/api/campaigns/${this.sessionId}/rolls`)
            .then(response => {
                let patch = [];
                response.data.forEach(roll => {
                    patch.push({ op: 'add', path: '/global/rollLog/-', value: roll });
                });
                this.store.dispatch('gamestate/applyDelta', patch);
            })
            .catch(error => {
                console.error(error);
            });
        axios
            .get(`/api/collections/scenes/campaign/${this.sessionId}`)
            .then(async response => {
                await this.loadScenes(response.data);
            })
            .catch(error => {
                console.error(error);
            });
        this.store.dispatch('gamestate/setBase');
    }

    onConfirmDelta(sessionDelta) {
        this.store.dispatch('gamestate/applyDelta', sessionDelta.delta);
        // When a new delta is applied, a new timeline is created so all undone deltas are reset.
        this.store.commit('gamestate/resetUndoneDeltas');
    }

    onCharacterAdded(characterDto) {
        this.store.dispatch('gamestate/entities/load', [characterDto]);
        Asset.loadAll([characterDto]);
        this.store.commit('gamestate/resetUndoneDeltas');
    }

    onCharacterRemoved(characterId) {
        const patch = { op: 'remove', path: `/entities/${characterId}` };
        console.log('onCharRemoved');
        this.store.dispatch('gamestate/patch', patch);
        this.store.dispatch('gamestate/entities/remove', characterId);
        this.store.commit('gamestate/resetUndoneDeltas');
    }

    onExceptionRaised(error) {
        console.err('Exception raised by server: ' + error);
    }

    // the submitted delta should be an array of jsonpatches, i.e. an array of arrays of jsonpatch operations
    submitDelta(submittedDelta) {
        // If submitted delta is just a single JSONpatch operation, wrap it in the structure
        if (!Array.isArray(submittedDelta)) submittedDelta = [[submittedDelta]];
        // Check if every value of the array is an array with objects in it.
        submittedDelta.forEach((jsonpatch, idx) => {
            let wrappedJsonpatch = !Array.isArray(jsonpatch) ? [jsonpatch] : jsonpatch;

            wrappedJsonpatch.forEach(operation => {
                if (typeof operation !== 'object') throw new TypeError('JSONPatch of submitted delta is malformed');
            });
            submittedDelta[idx] = wrappedJsonpatch;
        });
        this.tryDelta({ delta: submittedDelta });
    }
    async tryDelta(delta) {
        await this.connection.invoke('submitDeltaTemp', delta);
    }

    submitUndo() {
        let undoObject = 'undo placeholder';
        this.tryUndo(undoObject);
    }
    async tryUndo(undoObject) {
        await this.connection.invoke('submitUndo', undoObject);
    }
    undo() {
        this.store.dispatch('gamestate/undo');
    }

    submitRedo() {
        let redoObject = 'redo placeholder';
        this.tryRedo(redoObject);
    }
    async tryRedo(redoObject) {
        await this.connection.invoke('submitRedo', redoObject);
    }
    redo() {
        this.store.dispatch('gamestate/redo');
    }

    moveToken(id, newPos) {
        const patch = [{ op: 'replace', path: '/token/pos', value: newPos }];

        this.tryStep({ entities: [{ id, patch }] });
    }

    updateToken(id, patch) {
        this.tryStep({ entities: [{ id, patch }] });
    }

    async tryStep(step) {
        step.campaignId = this.sessionId;
        await this.connection.invoke('submitDelta', step);
    }

    async addCharacter(image, pos, sceneId) {
        const request = { campaignId: this.sessionId, x: pos.q, y: pos.r, image, sceneId };
        await this.connection.invoke('AddCharacter', request);
    }

    async removeCharacter(characterId) {
        const request = { campaignId: this.sessionId, characterId: characterId };
        await this.connection.invoke('RemoveCharacter', request);
    }

    submitRoll(diceObject) {
        this.tryRollDice(diceObject);
    }
    async tryRollDice(diceObject) {
        //TODO Check for valid diceObject
        await this.connection.invoke('rollDice', diceObject);
    }
    onReceiveDiceResult(diceResult) {
        let patch = { op: 'add', path: '/global/rollLog/-', value: diceResult };
        this.store.dispatch('gamestate/applyDelta', patch);
    }

    async updateScene(sceneId, patch) {
        const payload = { collection: COLLECTION_TYPES.scenes, campaignId: this.sessionId, id: sceneId, patch };
        await this.connection.invoke('UpdateObject', payload);
    }

    async addScene(scene) {
        await this.connection.invoke('AddCollectionItem', COLLECTION_TYPES.scenes, this.sessionId, scene);
    }

    async deleteScene(sceneId) {
        const success = await this.connection.invoke('RemoveObject', COLLECTION_TYPES.scenes, sceneId);
        if (!success) {
            console.warn(`Scene ${sceneId} was not deleted`);
        }
    }

    onObjectUpdated(parameters) {
        if (parameters.collection == COLLECTION_TYPES.scenes) {
            this.store.dispatch('collections/patch', parameters);
        }
    }

    onObjectAdded(collection, scene) {
        if (collection == COLLECTION_TYPES.scenes) {
            this.store.dispatch('collections/add', { collection, item: scene });
        }
    }

    onObjectRemoved(collection, id) {
        if (collection == COLLECTION_TYPES.scenes) {
            this.store.dispatch('collections/remove', { collection, id });
        }
    }

    async loadScenes(scenes) {
        const images = getAllImages(scenes);
        const promises = Asset.loadAll(images);
        Promise.all(promises).then(async () => {
            await this.store.dispatch('collections/load', { collection: COLLECTION_TYPES.scenes, items: scenes });
            this.store.dispatch('gamestate/activateScene', scenes[0]);
        });
    }
}

export { LivePlayDirector as default };
