using Microsoft.AspNetCore.Mvc;

namespace MythicTable.Extensions.Controllers
{
    public static class ControllerExtensions
    {
        public static string GetUserId(this ControllerBase controller)
        {
            return controller.HttpContext.User.FindFirst("name")?.Value;
        }
    }
}
