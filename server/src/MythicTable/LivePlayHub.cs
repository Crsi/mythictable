﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using MythicTable.Campaign.Data;
using MythicTable.Campaign.Util;
using MythicTable.GameSession;
using Dice;
using System;
using MythicTable.Scene.Data;
using MythicTable.Collections.Providers;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.JsonPatch;
using MythicTable.Collections.Data;

namespace MythicTable
{
    public class LivePlayHub : Hub<ILiveClient>
    {
        private IEntityCollection EntityCollection { get; }
        private IGameState GameState { get; }
        private ICampaignProvider CampaignProvider { get; }
        private ICollectionProvider CollectionProvider { get; }
        private readonly ILogger logger;

        public LivePlayHub(IEntityCollection entities, IGameState gamestate, ICampaignProvider campaignProvider, ICollectionProvider collectionProvider, ILogger<LivePlayHub> logger)
        {
            EntityCollection = entities;
            GameState = gamestate;
            CampaignProvider = campaignProvider;
            CollectionProvider = collectionProvider;
            this.logger = logger;
        }

        // TODO: #17: A difference between the GameState on the server and the GameState for player clients exists. This means a delta should not just be broadcast once applied
        // The GM client will resolve actions and submit 1 delta for the players and another delta for the server
        [Authorize]
        [HubMethodName("submitDelta")]
        public async Task<bool> UpdateCharacter(SessionDelta delta)
        {
            // TODO #6: This should call the GameState ApplyDelta. EntityCollection functionality should be ported and generalised.
            var success = await EntityCollection.ApplyDelta(delta.Entities);

            await InternalUpdateCharacter(delta);
            
            if (success)
            {
                await Clients.All.ConfirmDelta(delta);
            }

            return success;
        }

        [Authorize]
        public async Task<CharacterDTO> AddCharacter(AddCharacterRequest request)
        {
            var character = CharacterUtil.CreateCharacter(request.sceneId, request.image, request.x, request.y);
            character = await CampaignProvider.AddCharacter(request.campaignId, character);
            await Clients.All.CharacterAdded(character);
            return character;
        }

        [Authorize]
        public async Task<bool> RemoveCharacter(RemoveCharacterRequest request)
        {
            await CampaignProvider.RemoveCharacter(request.CampaignId, request.CharacterId);
            await Clients.All.CharacterRemoved(request.CharacterId);
            return true;
        }

        [Authorize]
        [HubMethodName("rollDice")]
        public async Task<bool> RollDice(RollDTO roll)
        {
            try
            {
                RollResult result = Roller.Roll(roll.Formula);
                roll.Result = result.ToString();
                this.logger.LogInformation($"Dice Roll - User: {roll.UserId} Roll: {roll.Formula} Results: {roll.Result}");
                var campaignId = roll.SessionId;
                await CampaignProvider.AddRoll(campaignId, roll);
                await Clients.All.ReceiveDiceResult(roll);
            }
            catch (DiceException)
            {
                string exception = "Invalid dice roll";
                await Clients.Caller.ExceptionRaised(exception);
            }

            return true;
        }

        [Authorize]
        [HubMethodName("submitDeltaTemp")]
        public async Task<bool> RebroadcastDeltaTemp(SessionOpDelta delta)
        {
            // TODO #6: This should call the GameState ApplyDelta. EntityCollection functionality should be ported and generalised.
            var success = await GameState.ApplyDelta(delta);

            if (success)
            {
                await Clients.All.ConfirmOpDelta(delta);
            }

            return success;
        }

        [Authorize]
        [HubMethodName("submitUndo")]
        public async Task<bool> RebroadcastUndo(string undoPlaceHolder)
        {
            var success = await GameState.Undo(undoPlaceHolder);

            if (success)
            {
                await Clients.All.Undo();
            }

            return success;
        }

        [Authorize]
        [HubMethodName("submitRedo")]
        public async Task<bool> RebroadcastRedo(string redoPlaceholder)
        {
            var success = await this.GameState.Redo(redoPlaceholder);

            if (success)
            {
                await Clients.All.Redo();
            }

            return success;
        }

        [Authorize]
        public async Task<JObject> AddCollectionItem(string collection, string campaignId, JObject item)
        {
            var obj = await CollectionProvider.CreateByCampaign(this.GetUserId(), collection, campaignId, item);
            await Clients.All.ObjectAdded(collection, obj);
            return obj;
        }

        [Authorize]
        public async Task<JObject> UpdateObject(UpdateCollectionHubParameters parameters)
        {
            if (await CollectionProvider.UpdateByCampaign(parameters.Collection, parameters.CampaignId, parameters.Id, parameters.Patch) > 0)
            {
                await Clients.All.ObjectUpdated(parameters);
                return await CollectionProvider.GetByCampaign(parameters.Collection, parameters.CampaignId, parameters.Id);
            }
            return null;
        }

        [Authorize]
        public async Task<bool> RemoveObject(string collection, string id)
        {
            if (await CollectionProvider.Delete(this.GetUserId(), collection, id) > 0)
            {
                await Clients.All.ObjectRemoved(collection, id);
                return true;
            }
            return false;
        }

        private async Task<bool> InternalUpdateCharacter(SessionDelta delta)
        {
            var campaignId = delta.CampaignId;
            foreach(var entity in delta.Entities)
            {
                this.logger.LogInformation($"Entity: {entity.EntityId}");
                await this.CampaignProvider.UpdateCharacter(campaignId, entity.EntityId, entity.Patch);
            }
            return true;
        }

        private string GetUserId()
        {
            return this.Context.User.FindFirst("name")?.Value;
        }
    }
}
