using MythicTable.Common.Exceptions;

namespace MythicTable.Campaign.Exceptions
{
    public class CollectionInvalidException : MythicTableException
        {
            public CollectionInvalidException(string msg): base(msg) {}
        }
}