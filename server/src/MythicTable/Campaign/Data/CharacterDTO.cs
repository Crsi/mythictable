using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Mongo.Migration.Documents;
using Mongo.Migration.Documents.Attributes;
using MythicTable.Common.Data;
using Newtonsoft.Json;

namespace MythicTable.Campaign.Data
{
    [RuntimeVersion("0.0.2")]
    public class CharacterDTO: IDocument
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [JsonProperty("id")]
        public string Id { get; set; }

        public DocumentVersion Version { get; set; }

        [BsonElement("token")]
        [JsonProperty("token")]
        [JsonConverter(typeof(BsonConverter))]
        public BsonDocument Token { get; set; }

        [BsonElement("asset")]
        [JsonProperty("asset", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(BsonConverter))]
        public BsonDocument Asset { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("attributes")]
        [JsonProperty("attributes", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(BsonConverter))]
        public BsonDocument Attributes { get; set; }
    }
}