using Newtonsoft.Json.Linq;

namespace MythicTable.Campaign.Util
{
    public class SceneUtil
    {
        public static JObject CreateScene(string image, double q, double r)
        {
            return JObject.Parse($@"{{
                id: 'debug',
                scene: {{ stage: '.' }},
                stage: {{
                    grid: {{ type: 'square', size: 50 }},
                    bounds: {{
                        nw: {{ q: 0, r: 0 }},
                        se: {{ q: {q}, r: {r} }},
                    }},
                    color: '#223344',
                    elements: [ {{
                        id: 'background',
                        asset: {{ kind: 'image', src: '{image}' }},
                        pos: {{ q: 0, r: 0, pa: '00' }},
                    }},
                    ],
                }},
            }}");
        }
    }
}
