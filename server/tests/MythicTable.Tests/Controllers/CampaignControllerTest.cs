using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Campaign.Controllers;
using MythicTable.Campaign.Data;
using MythicTable.Campaign.Exceptions;
using MythicTable.Collections.Providers;
using Xunit;


namespace MythicTable.Tests.Controllers
{
    public class CampaignControllerTest : IAsyncLifetime
    {
        private const string DoesntExistId = "551137c2f9e1fac808a5f572";
        private CampaignController controller;
        private ICampaignProvider campaignProvider;

        private ICollectionProvider collectionProvider;

        private string User { get; set; } = "Jon";

        public Task InitializeAsync()
        {
            campaignProvider = new InMemoryCampaignProvider();
            collectionProvider = new InMemoryCollectionProvider(Mock.Of<ILogger<InMemoryCollectionProvider>>());
            controller = new CampaignController(campaignProvider, collectionProvider);

            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(hc => hc.User.FindFirst(It.IsAny<string>()))
                           .Returns(() => {
                               return new Claim("", User);
                            });
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            return Task.CompletedTask;
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public void TestConstructorWithNullCampaignProviderThrowsException()
        {
            Func<CampaignController> construction = () => new CampaignController(null, null);

            Assert.Throws<ArgumentNullException>("campaignProvider", construction);
        }

        [Fact]
        public async void TestPostSucceedsAsync()
        {
            var testCampaign = new CampaignDTO
            {
                Name = "Test Campaign"
            };

            var response = await controller.PostCampaign(testCampaign);
            var actionResult = response.Result as CreatedAtActionResult;
            Assert.Equal(201, actionResult.StatusCode);
            var resultCampaign = actionResult.Value as CampaignDTO;
            Assert.Equal(testCampaign.Name, resultCampaign.Name);
        }

        [Fact]
        public async void TestPostSetsUserAsOwnerAsync()
        {
            User = "test user";
            var testCampaign = new CampaignDTO
            {
                Name = "Test Campaign"
            };

            var response = await controller.PostCampaign(testCampaign);
            var actionResult = response.Result as CreatedAtActionResult;
            Assert.Equal(201, actionResult.StatusCode);
            var resultCampaign = actionResult.Value as CampaignDTO;
            Assert.Equal("test user", resultCampaign.Owner);
        }

        [Fact]
        public async void TestPostReturnsBadRequestForNullCampaignsAsync()
        {
            Func<Task> postCampaignWithoutId = async () => await controller.PostCampaign(null);
            await Assert.ThrowsAsync<CampaignInvalidException>(postCampaignWithoutId);
        }

        [Fact]
        public async void TestPutSucceedsAsync()
        {
            var testCampaign = await CreateAndPostTestCampaign();

            testCampaign.Name = "Modified";
            var putResponse = await controller.PutCampaign(testCampaign) as NoContentResult;
            Assert.Equal(204, putResponse.StatusCode);
        }

        [Fact]
        public async void TestPutReturnsBadRequestForNullCampaignAsync()
        {
            Func<Task> putCampaignWithoutId = async () => await controller.PutCampaign(null);
            await Assert.ThrowsAsync<CampaignInvalidException>(putCampaignWithoutId);
        }

        [Fact]
        public async void TestPutReturnsBadRequestForBadCampaignIdAsync()
        {
            Func<Task> putCampaignWithoutId = async () => await controller.PutCampaign(
                new CampaignDTO
                {
                    Name = "Test Campaign"
                });

            await Assert.ThrowsAsync<CampaignInvalidException>(putCampaignWithoutId);
        }

       [Fact]
        public async void TestGetReturnsValidResultAsync()
        {
            var testCampaign = await CreateAndPostTestCampaign();

            var getResult = await controller.GetCampaign(testCampaign.Id) as ActionResult<CampaignDTO>;
            Assert.Equal(testCampaign.Name, getResult.Value.Name);
            Assert.Equal(testCampaign.Id, getResult.Value.Id);
        }

        [Fact]
        public async void TestGetReturnsNoCampaignsAsync()
        {
            Func<Task> getUnknownCampaign = async () => await controller.GetCampaign(DoesntExistId);
            await Assert.ThrowsAsync<CampaignNotFoundException>(getUnknownCampaign);
        }

        [Fact]
        public async void TestGetMultipleReturnsValidResultAsync()
        {
            var getResult1 = await controller.GetCampaigns() as ActionResult<List<CampaignDTO>>;
            var testCampaign1 = await CreateAndPostTestCampaign();
            var testCampaign2 = await CreateAndPostTestCampaign();
            var getResult2 = await controller.GetCampaigns() as ActionResult<List<CampaignDTO>>;

            var campaign1 = getResult1.Value as List<CampaignDTO>;
            var campaign2 = getResult2.Value as List<CampaignDTO>;
            // TODO this will be more graceful once we can clear out the mongodb on each run
            Assert.Equal(2, campaign2.Count - campaign1.Count);
            Assert.Equal(testCampaign1.Id, campaign2[campaign2.Count-2].Id);
            Assert.Equal(testCampaign1.Name, campaign2[campaign2.Count-2].Name);
            Assert.Equal(testCampaign2.Id, campaign2[campaign2.Count-1].Id);
            Assert.Equal(testCampaign2.Name, campaign2[campaign2.Count-1].Name);
        }

        [Fact]
        public async void TestDeleteAsync()
        {
            var testCampaign = await CreateAndPostTestCampaign();
            var response = await controller.DeleteCampaign(testCampaign.Id) as ActionResult<CampaignDTO>;
            Assert.Equal(testCampaign.Name, response.Value.Name);
            Assert.Equal(testCampaign.Id, response.Value.Id);
        }

        [Fact]
        public async void TestDeleteInvalidIdFailsAsync()
        {
            Func<Task> deleteUnknownCampaign = async () => await controller.DeleteCampaign(DoesntExistId);
            await Assert.ThrowsAsync<CampaignNotFoundException>(deleteUnknownCampaign);
        }

        [Fact]
        public async void TestJoinNonExistantCampaignFailsAsync()
        {
            Func<Task> joinNonExistantCampaign = async () => await controller.Join(DoesntExistId);
            await Assert.ThrowsAsync<CampaignNotFoundException>(joinNonExistantCampaign);
        }

        [Fact]
        public async void TestJoinAddsPlayerAsync()
        {
            User = "owner";
            var testCampaign = await CreateAndPostTestCampaign();
            User = "player1";
            var response = await controller.Join(testCampaign.Id);
            var getResult = response as ActionResult<CampaignDTO>;
            Assert.Equal(testCampaign.Name, getResult.Value.Name);
            Assert.Equal(testCampaign.Id, getResult.Value.Id);
            var campaigns = getResult.Value.Players;
            Assert.Single(campaigns);
            Assert.Equal("player1", campaigns[0].Name);
        }

        [Fact]
        public async void TestLeaveNonExistantCampaignFailsAsync()
        {
            Func<Task> leaveUnknownCampaign = async () => await controller.Leave(DoesntExistId);
            await Assert.ThrowsAsync<CampaignNotFoundException>(leaveUnknownCampaign);
        }

        [Fact]
        public async void TestLeaveCampaignNotInFailsAsync()
        {
            User = "owner";
            var testCampaign = await CreateAndPostTestCampaign();
            User = "player1";
            Func<Task> leaveNonParticipatoryCampaign = async () => await controller.Leave(testCampaign.Id);
            await Assert.ThrowsAsync<CampaignRemovePlayerException>(leaveNonParticipatoryCampaign);
        }

        [Fact]
        public async void TestLeaveRemovesPlayerAsync()
        {
            User = "owner";
            var testCampaign = await CreateAndPostTestCampaign();
            User = "player1";
            await controller.Join(testCampaign.Id);
            var response = await controller.Leave(testCampaign.Id);
            var getResult = response as ActionResult<CampaignDTO>;
            var campaigns = getResult.Value.Players;
            Assert.Empty(campaigns);
        }

        [Fact]
        public async void TestIssue48CreateDefaultCharactersAsync()
        {
            var testCampaign = await CreateAndPostTestCampaign();
            var results = await controller.GetCharacters(testCampaign.Id);
            
            Assert.Equal(10, results.Count);
        }

        [Fact]
        public async void TestIssue48CharactersShouldBeInTheDBAsync()
        {
            var testCampaign = await CreateAndPostTestCampaign();
            var results = await campaignProvider.GetCharacters(testCampaign.Id);
            
            Assert.Equal(10, results.Count);
        }

        private async Task<CampaignDTO> CreateAndPostTestCampaign()
        {
            var testCampaign = new CampaignDTO
            {
                Name = "Test Campaign"
            };

            var response = await controller.PostCampaign(testCampaign);
            var actionResult = response.Result as CreatedAtActionResult;
            Assert.Equal(201, actionResult.StatusCode);
            return (CampaignDTO)actionResult.Value;
        }
    }
}